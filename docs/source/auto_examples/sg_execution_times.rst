
:orphan:

.. _sphx_glr_auto_examples_sg_execution_times:

Computation times
=================
**21:54.097** total execution time for **auto_examples** files:

+-------------------------------------------------------------------------------------------------------------+-----------+-----------+
| :ref:`sphx_glr_auto_examples_plot_equalization_classification.py` (``plot_equalization_classification.py``) | 18:29.698 | 173.6 MB  |
+-------------------------------------------------------------------------------------------------------------+-----------+-----------+
| :ref:`sphx_glr_auto_examples_plot_equalization_clustering.py` (``plot_equalization_clustering.py``)         | 03:21.514 | 1172.0 MB |
+-------------------------------------------------------------------------------------------------------------+-----------+-----------+
| :ref:`sphx_glr_auto_examples_plot_data_visualization.py` (``plot_data_visualization.py``)                   | 00:02.885 | 59.2 MB   |
+-------------------------------------------------------------------------------------------------------------+-----------+-----------+
| :ref:`sphx_glr_auto_examples_data_extraction.py` (``data_extraction.py``)                                   | 00:00.000 | 0.0 MB    |
+-------------------------------------------------------------------------------------------------------------+-----------+-----------+
