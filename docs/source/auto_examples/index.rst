:orphan:

.. _notebooks:

Notebooks
================

This section contains the notebooks with the code and detailed comments used to generate the results in the paper.



.. raw:: html

    <div class="sphx-glr-thumbnails">


.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="    The audio chunks are the output of the software noise-extractor on the audio files provided...">

.. only:: html

  .. image:: /auto_examples/images/thumb/sphx_glr_data_extraction_thumb.png
    :alt: Data extraction

  :ref:`sphx_glr_auto_examples_data_extraction.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Data extraction</div>
    </div>


.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="This notebook shows the distribution of the datasets from datasets. The center of interest is t...">

.. only:: html

  .. image:: /auto_examples/images/thumb/sphx_glr_plot_data_visualization_thumb.png
    :alt: Data Visualization

  :ref:`sphx_glr_auto_examples_plot_data_visualization.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Data Visualization</div>
    </div>


.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="In this notebook is described the clusterization process of datasets identified in clustering-d...">

.. only:: html

  .. image:: /auto_examples/images/thumb/sphx_glr_plot_equalization_clustering_thumb.png
    :alt: Cluster analysis

  :ref:`sphx_glr_auto_examples_plot_equalization_clustering.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Cluster analysis</div>
    </div>


.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="This notebook shows the performance of classification algorithms on the datasets from datasets.">

.. only:: html

  .. image:: /auto_examples/images/thumb/sphx_glr_plot_equalization_classification_thumb.png
    :alt: Classification analysis

  :ref:`sphx_glr_auto_examples_plot_equalization_classification.py`

.. raw:: html

      <div class="sphx-glr-thumbnail-title">Classification analysis</div>
    </div>


.. raw:: html

    </div>


.. toctree::
   :hidden:

   /auto_examples/data_extraction
   /auto_examples/plot_data_visualization
   /auto_examples/plot_equalization_clustering
   /auto_examples/plot_equalization_classification


.. only:: html

  .. container:: sphx-glr-footer sphx-glr-footer-gallery

    .. container:: sphx-glr-download sphx-glr-download-python

      :download:`Download all examples in Python source code: auto_examples_python.zip </auto_examples/auto_examples_python.zip>`

    .. container:: sphx-glr-download sphx-glr-download-jupyter

      :download:`Download all examples in Jupyter notebooks: auto_examples_jupyter.zip </auto_examples/auto_examples_jupyter.zip>`
